# thinkers-book-club

Monthly discussions about deep books and videos (especially tech and AI and neuroscience, mostly fiction and speculative nonfiction)

- [ ] [The Authoritarian Personality](https://en.wikipedia.org/wiki/The_Authoritarian_Personality) by Theodor W. Adorno - NF sociology
- [ ] [The Wubi Effect](https://www.wnycstudios.org/podcasts/radiolab/articles/wubi-effect) Radiolab episode about Wang Yongmin who fit 70k Chinese characters onto a keyboard
- [ ] _How I Built This_ by Guy Raz [book](https://www.guyraz.com/) and [podcast](https://www.npr.org/podcasts/510313/how-i-built-this) 
- [ ] [3 Body Problem](https://en.wikipedia.org/wiki/The_Three-Body_Problem_(novel)) by Cixin Liu, SciFi trilogy about alien intelligence and AI
- [ ] [The Lifecycle of Software Objects](https://en.wikipedia.org/wiki/The_Lifecycle_of_Software_Objects) by Ted Chiang SciFi short story in _Exhalation_ about AI humanoid pets/children and their lifecycle (when corporations that made them go under)
- [ ] _After On: A Silicon Valley Novel_, and [podcast](https://after-on.com/) by Rob Reid - near futurism about AI and how it manipulates us and its affect on Silicon Valley board room decisions/politics
- [ ] [Blind Sight](https://en.wikipedia.org/wiki/Blindsight_(Watts_novel) SciFi novel & neuroscience of consciousness by Peter Watts
- [ ] [Supercooperators](https://www.abc.net.au/radionational/programs/latenightlive/super-cooperators/2948402) by Nowak nonfiction
- [ ] [Mathematical Models of Social Evolution: A Guide for the Perplexed](https://xcelab.net/rm/book/) by Richard McElreath and Robert Boyd
- [ ] [Intuition Pumps](https://en.wikipedia.org/wiki/Intuition_pump) by Daniel Dennet
- [ ] Impossible Conversations, by [Peter Boghossian](https://en.wikipedia.org/wiki/Peter_Boghossian#Street_epistemology)
- [ ] anything by Andrew Yang
- [ ] The selfish gene, by Dawkins
- [ ] Lex Friedman interviews with Zev Weinstein, Magnolia Kelis, Avi Loeb, Lisa Feldman Barret, Matthew Johnson, Wolfram
- [ ] [AI: A Guide for Thinking Humans](https://en.wikipedia.org/wiki/Artificial_Intelligence:_A_Guide_for_Thinking_Humans)
- [ ] [Complexity: A Guided Tour](https://www.barnesandnoble.com/w/complexity-melanie-mitchell/1117028990) by Melanie Mitchell
- [ ] Sam Harris podcasts
- [ ] Hidden Brain episodes
